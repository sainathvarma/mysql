package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping("helloMethod")
	public String helloMethod(){
		return "hello method from hellocontroller";
	}
	
	
}