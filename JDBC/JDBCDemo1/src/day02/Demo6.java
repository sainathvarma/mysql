package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

//Select all Employee Records
public class Demo6 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		 
		String selectQuery = "SELECT * FROM employee";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
				int empId = resultSet.getInt("empId");
				String empName = resultSet.getString("empName");
				double salary = resultSet.getDouble("salary");
				String gender = resultSet.getString("gender");
				String emailId = resultSet.getString("emailId");
				
				System.out.println("Employee ID: " + empId + ", Name: " + empName + ", Salary: " + salary + ", Gender: " + gender + ", Email ID: " + emailId);
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}
